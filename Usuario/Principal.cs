﻿using System;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using System.Data;
using System.Security.Cryptography;
using Android.Provider;
using Uri = Android.Net.Uri;
using Path = System.IO.Path;
using Plugin.Geolocator;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Android.Graphics;
using Android.Runtime;
using Android;
using Plugin.CurrentActivity;
using Android.Support.V4.Content;
using Android.Content.PM;
using Android.Support.V4.App;

namespace Usuario
{
    [Activity(Label = "Principal")]
    public class Principal : Activity
    {
        Uri FileUri;
        int camrequestcode = 100;
        ImageView Imagen;
        FileStream streamFile;
        double lat, lon;
        Intent intent;
        EditText txtFolio, txtDomicilio, txtNombre, txtCorreo, txtEdad, txtSaldo;
        Button btnGuardar, btnBuscar;
        protected override void OnStart()
        {
            base.OnStart();
            const string permission = Manifest.Permission.Camera;

            if (ContextCompat.CheckSelfPermission(this, permission) != Android.Content.PM.Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.WriteExternalStorage }, 0);
            }

        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
          
            SetContentView(Resource.Layout.vistaPrincipal);
            CrossCurrentActivity.Current.Init(this, bundle);
            var lblDestino = FindViewById<TextView>(Resource.Id.textView2);
             Imagen = FindViewById<ImageView>(Resource.Id.imageview);
             txtFolio = FindViewById<EditText>(Resource.Id.txtfolio);
             txtDomicilio = FindViewById<EditText>(Resource.Id.txtdomicilio);
             txtNombre = FindViewById<EditText>(Resource.Id.txtnombre);
             txtCorreo = FindViewById<EditText>(Resource.Id.txtcorreo);
             txtEdad = FindViewById<EditText>(Resource.Id.txtedad);
             txtSaldo = FindViewById<EditText>(Resource.Id.txtsaldo);
             btnGuardar = FindViewById<Button>(Resource.Id.btnguardar);
             btnBuscar = FindViewById<Button>(Resource.Id.btnbuscar);
            string Usuari;
            Usuari = Intent.GetStringExtra("Usuario");
            lblDestino.Text = Usuari;
              //  ArchivoImagen("https://laverdadnoticias.com/__export/1595680696728/sites/laverdad/img/2020/07/25/shrek_whatsapp_pelxcula_stickers_viral.jpg_423682103.jpg");
            async void ArchivoImagen(string url)
            {
                var ruta = await DescargarImagen(url);
                Android.Net.Uri rutaImagen = Android.Net.Uri.Parse(ruta);
                Imagen.SetImageURI(rutaImagen);
            }
            btnGuardar.Click += async delegate
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);
                    lon = position.Longitude;
                    lat = position.Latitude;
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
               

              
            };
            btnBuscar.Click += delegate
            {
              
            };
            Imagen.Click += delegate
            {
                try
                {
                    if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == (int)Permission.Granted)
                    {
                        intent = new Intent(MediaStore.ActionImageCapture);
                        intent.PutExtra(MediaStore.ExtraOutput, FileUri);
                        StartActivityForResult(intent, camrequestcode,bundle);
                    }
                    else
                    {
                        Toast.MakeText(this.ApplicationContext, "No hay permiso para la camara bro", ToastLength.Long).Show();
                    }
                  
                }
                catch (Exception ex)
                {

                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
            };
        }
       
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == camrequestcode)
            {
               
                    if (txtNombre.Text!=null)
                    {
                        FileUri = Android.Net.Uri.Parse(Path.Combine(System.Environment.GetFolderPath(
                            System.Environment.SpecialFolder.Personal), txtNombre.Text + ".jpg"));
                        if (File.Exists(FileUri.ToString()))
                        {
                            Toast.MakeText(this.ApplicationContext, "La imagen ya existe", ToastLength.Long).Show();
                        }
                        else
                        {
                            streamFile = new FileStream(Path.Combine(System.Environment.GetFolderPath
                            (System.Environment.SpecialFolder.Personal), txtNombre.Text + ".jpg"), FileMode.Create);                      
                            var bitmapImagen = (Android.Graphics.Bitmap)data.Extras.Get("data");
                            bitmapImagen.Compress(Bitmap.CompressFormat.Jpeg, 100, streamFile);
                            streamFile.Close();
                            Imagen.SetImageBitmap(bitmapImagen);
                        }
                    }
             

                   
                
            }
        }
        public async Task<string> DescargarImagen(string url)
        {
            WebClient client = new WebClient();
            byte[] imageData = await client.DownloadDataTaskAsync
                (url);
            string documentspath = System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal);
            string localfilename = "foto1.jpg";
            string localpath = Path.Combine(documentspath, localfilename);
            File.WriteAllBytes(localpath, imageData);
            return localpath;
        }
        public string CifradoAES(string textoacifrar,
            string password, string vectordeinicio, int saltos)
        {
            AesManaged aes = null;
            MemoryStream streamenmemoria = null;
            CryptoStream streamcifrado = null;
            try
            {
                var rfc2898 = new Rfc2898DeriveBytes
                    (password, Encoding.UTF8.GetBytes(vectordeinicio), saltos);
                aes = new AesManaged();
                aes.Key = rfc2898.GetBytes(32);
                aes.IV = rfc2898.GetBytes(16);
                streamenmemoria = new MemoryStream();
                streamcifrado = new CryptoStream
                    (streamenmemoria, aes.CreateEncryptor(), CryptoStreamMode.Write);
                byte[] datos = Encoding.UTF8.GetBytes(textoacifrar);
                streamcifrado.Write(datos, 0, datos.Length);
                streamcifrado.FlushFinalBlock();
                return Convert.ToBase64String(streamenmemoria.ToArray());
            }
            catch (Exception e)
            {
                return "error";
            }
            finally
            {
                if (streamcifrado != null)
                    streamcifrado.Close();
                if (streamenmemoria != null)
                    streamenmemoria.Close();
                if (aes != null)
                    aes.Clear();
            }
        }
        public string DecifradoAES(string textoadescifrar,
            string password, string vectordeinicio, int saltos)
        {
            AesManaged aes = null;
            MemoryStream streamenmemoria = null;
            try
            {
                var rfc2898 = new Rfc2898DeriveBytes
                    (password, Encoding.UTF8.GetBytes(vectordeinicio), saltos);
                aes = new AesManaged();
                aes.Key = rfc2898.GetBytes(32);
                aes.IV = rfc2898.GetBytes(16);
                streamenmemoria = new MemoryStream();
                CryptoStream streamcifrado = new CryptoStream
                    (streamenmemoria, aes.CreateDecryptor(), CryptoStreamMode.Write);
                byte[] datos = Convert.FromBase64String(textoadescifrar);
                streamcifrado.Write(datos, 0, datos.Length);
                streamcifrado.FlushFinalBlock();
                byte[] arreglodescifrado = streamenmemoria.ToArray();
                if (streamcifrado != null)
                    streamcifrado.Dispose();
                return Encoding.UTF8.GetString
                    (arreglodescifrado, 0, arreglodescifrado.Length);
            }
            catch (Exception e)
            {
                return "error";
            }
            finally
            {
                if (streamenmemoria != null)
                    streamenmemoria.Close();
                if (aes != null)
                    aes.Clear();
            }
        }
    }
    
    public class Empleado:TableEntity
    {
        public Empleado(string categoria, string nombre)
        {
            PartitionKey = categoria;
            RowKey= nombre;
        }
        public int Folio { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Domicilio { get; set; }
        public int Edad { get; set; }
        public string Saldo { get; set; }
        public string Imagen { get; set; }
    }


}