﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;
using System.IO;
using Android.Content;
using System.Security.Cryptography;
using System.Text;

namespace Usuario
{
    [Activity(Label = "Guardando en Azure", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string Usuari, Password;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            var btnIngresa = FindViewById<Button>(Resource.Id.btningresar);
            var txtUser = FindViewById<EditText>(Resource.Id.txtusuario);
            var txtPass = FindViewById<EditText>(Resource.Id.txtpassword);
            var Imagen = FindViewById<ImageView>(Resource.Id.imageview);
            Imagen.SetImageResource(Resource.Drawable.logo);
            btnIngresa.Click += delegate
            {
                Cargar();
                /*
                try
                {
                    var WS = new ServicioWeb.ServicioWeb();
                    Usuari = txtUser.Text;
                    Password = CifradoSHA(txtPass.Text);
                    //passc = WS.Login(txtUser.Text).ToString();
                    string pass = WS.Login(txtUser.Text).Tables[0].Rows[0]["Password"].ToString();
                    if (Password.Equals(pass))
                        Cargar();

                    else
                        Toast.MakeText(this, "Error de Password", ToastLength.Short).Show();
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                }
                */
            };
        }
        public void Cargar()
        {
            Intent Destino = new Intent(this, typeof(Principal));
            Destino.PutExtra("Usuario", Usuari);
            StartActivity(Destino);
        }



        //--------------------------MétodosCifrados----------------------------------
        public string CifradoSHA(string texto)
        {
            using (SHA256 hash = SHA256.Create())
            {
                byte[] arreglo = hash.ComputeHash(Encoding.UTF8.GetBytes(texto));
                StringBuilder cadenatexto = new StringBuilder();
                for (int i = 0; i < arreglo.Length; i++)
                {
                    cadenatexto.Append(arreglo[i].ToString("X2"));
                }
                return cadenatexto.ToString();
            }
        }

    }
}